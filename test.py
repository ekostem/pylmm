import matplotlib.pyplot as plt
import lmm as m
import scipy as sp
import scipy.stats as stats

lmm = m.lmm()
#lmm.readY("./data/xanthosine.raw.area.phen", 2)
#lmm.readX("./data/xanthosine.raw.area.cov", 2)
#lmm.readG("./data/xanthosine.raw.area.aIBS.kinf")

lmm.readY( "../pyZVT/data/HDL.pheno", 2 )
lmm.readX( "../pyZVT/data/HDL.covar", 2 )
lmm.readG( "../pyZVT/data/HDL.GW.K" )

#lmm.readY("./data/simPhen.1", 2)
#lmm.readX("./data/cov.txt", 2)
#lmm.readG("./data/2.1.kinship")
#lmm.readR("./data/2.1.BG.kinship")

#lmm.diagonalize_svd()
lmm.diagonalize()
lmm.computeConstants()
lmm.setY(0)
lmm.update(1.0)


optD = lmm.findOptDelta()
print optD
h = lmm.getHeritability( optD )
print """Genetic heritability: %.2f""" % h


#pred =  lmm.getBLUP( optD )

out = lmm.sweep()

fig = plt.figure()
ax = fig.add_subplot(2,1,1)
ax.plot( out[0,], out[1,] )
ax.axvline(x=optD)
ax.set_title('LL')

ax = fig.add_subplot(2,1,2)
ax.plot( out[0,], out[2,] )
ax.axvline(x=optD)
ax.set_title('dLL')
plt.show()
