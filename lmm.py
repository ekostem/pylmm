#(c) Copyright 2013 Emrah Kostem. All Rights Reserved.

#import numexpr as ne
import numpy as np
import scipy as sp
from scipy import linalg
from scipy import optimize
import math
import os
import sys

class lmmError(Exception):
    def __init__( self, value ):
        self.value = value

    def __str__( self ):
        return repr(self.value)


class lmm:
    def __init__( self ):
        self.n = 0 ## number of individuals
        self.p = 0 ## number of covariates
        self.numPhens = 0;
        self.currentPhenNo = 0;
        self.d = 1 ## initial delta
        self.constTerm = 0;
        self.m_allY = None ## (n x numPhens) phenotypes matrix

        self.m_Y = None ## (n x 1) Y matrix
        self.m_X = None ## (n x p) X matrix
        self.m_G = None ## (n x n) genetic covariance matrix
        self.m_R = None ## (n x n) noise covariance
        self.m_C = None ## (n x n) centering matrix
        self.m_allY_original = None
        self.m_X_original = None
        self.m_G_original = None
        self.m_R_original = None
        
        self.H = None
        
        self.varG = 0;
        self.varE = 0;

        self.eigenValues = None ## array of eigenvalues
        self.evals_plus_d = None
        self.m_Vinv = None ## sparse covariance matrix
        self.m_Xt_Vinv_X = None
        self.m_P = None ## P matrix
        self.YtPY = None

        self.is_R_iid = True
        self.is_Diagonalized = False

    def checkFile( self, filename ):
        if ( not os.path.exists(filename) ):
            raise lmmError("""Error reading file: %s""" % filename)
            sys.exit()

    def checkDiagonal( self ):
        if ( not self.is_Diagonalized ):
            raise lmmError("""First diagonalize the model""")

    def setY( self, n):
        self.checkDiagonal()
        self.currentPhenNo = n
        self.m_Y = self.m_allY[:, self.currentPhenNo]
        self.m_Y.shape = (self.n, 1)

    def setY_fromOriginal( self, n):
        self.m_allY = np.copy( self.m_allY_original )

    def readY( self, filename, numColSkip):
        ## Skips the first numColSkip columns
        ## handy if reading from plink format
        self.checkFile(filename)

        self.m_allY = np.genfromtxt(filename)
        n = self.m_allY.shape[1] ## many columns
        self.m_allY = self.m_allY[:, range(numColSkip, n)]
        self.numPhens = float(n - numColSkip)
        self.m_allY_original = np.copy(self.m_allY)
        if ( not self.n ):
            self.n = float(self.m_allY.shape[0])
        n = int(self.n)
        self.m_C = np.eye(n) - (1.0 / self.n) * np.ones( ( n, n ) ) 
    
    def readY_soft( self, matrix ):
        self.m_allY = np.copy( matrix )
        self.m_allY_original = np.copy( self.m_allY )
        if ( not self.n ):
            self.n = float(self.m_allY.shape[0])
        n = int(self.n)
        self.m_C = np.eye(n) - (1.0 / self.n) * np.ones( ( n, n ) ) 
        
    
    def readX( self, filename, numColSkip):
        ## Skips the first numColSkip columns
        self.checkFile(filename)

        self.m_X = np.genfromtxt(filename)
        n = self.m_X.shape[1] ## many columns
        self.m_X = self.m_X[:, range(numColSkip, n)]
        self.p = float(n - numColSkip)
        if ( not self.n ):
            self.n = float(self.m_X.shape[0])
        self.m_X_original = np.copy( self.m_X )

    def readX_soft( self, matrix ):
        self.m_X = np.copy( matrix )
        self.m_X_original = np.copy( self.m_X )
        self.p = float( matrix.shape[1] ) ## many columns
        if ( not self.n ):
            self.n = float( matrix.shape[0]) ## many individuals

    def setX_fromOriginal( self ):
        self.m_X = np.copy( self.m_X_original )

    def readG( self, filename ):
        self.checkFile(filename)
        #self.m_G = self.m_C * sp.matrix( np.genfromtxt( filename ) ) * self.m_C
        #self.m_G = sp.matrix(np.genfromtxt(filename))
        # THERE IS A MEMORY LEAK IN genfromtxt
        self.m_G = np.load( filename )
        self.m_R = np.eye( self.n )
        self.is_R_iid = True
        self.m_G_original = np.copy( self.m_G )
        self.m_R_original = np.copy( self.m_R )
        if ( self.n == 0):
            self.n = float( self.m_R.shape[0] )
        

    def readG_soft( self, matrix):
        #self.m_G = self.m_C * sp.matrix( matrix ) * self.m_C
        self.m_G = np.copy( matrix )
        self.m_R = np.eye( self.n )
        self.is_R_iid = True
        self.m_G_original = np.copy( self.m_G )
        self.m_R_original = np.copy( self.m_R )
        if ( self.n == 0):
            self.n = float( self.m_R.shape[0] )
        


    def readR( self, filename ):
        ## Noise covariance
        ## This kernel becomes the background 
        self.checkFile(filename)
        # THERE IS A MEMORY LEAK IN genfromtxt
        #self.m_R = sp.matrix(np.genfromtxt(filename))
        self.m_R = np.load( filename )
        self.isR_iid = False
        self.m_R_original = np.copy(self.m_R)
        if ( self.n == 0):
            self.n = float( self.m_R.shape[0] )

    def readR_soft( self, matrix):
        #self.m_R = self.m_C * sp.matrix( matrix ) * self.m_C
        self.m_R = np.copy( matrix )
        self.isR_iid = False
        self.m_R_original = np.copy( self.m_R )
        if ( self.n == 0):
            self.n = float( self.m_R.shape[0] )
        

    def make_R_iid( self ):
        if ( self.isR_iid ):
            return
        ##rotate everything to diagonalize the noise covariance
        ## first perform eigen decomposition
        e, H = linalg.eigh(self.m_R)
        i = np.arange( self.n, dtype=int )[ e < 1e-5 ]
        e[ i ] = 1e-5
        e = np.sqrt(e)
        e = 1.0 / e
        #e[i] = 0.0
        L = np.diag( e )
        R_sqrt = H.dot( L ).dot( H.T )
        ## first rotate by R_sqrt
        self.m_allY = R_sqrt.dot( self.m_allY ) 
        if ( self.m_X is not None):
            self.m_X = R_sqrt.dot( self.m_X )
        self.m_G = R_sqrt.dot( self.m_G ).dot( R_sqrt )
        self.m_R = np.eye( self.n )
        self.isR_iid = True


    def diagonalize_svd(self):
        """
        Use svd with tolerance eigenvalue
        """
        if ( not self.is_R_iid ):
            self.make_R_iid()

        tol = 1e-10
        self.H, e, Vt = linalg.svd(self.m_G)
        x = np.arange(self.n, dtype=int)[e > tol]
        self.H = self.H[:, x]
        self.n = len(x)
        self.m_allY = H.T.dot( self.m_allY )
        if ( self.m_X is not None):
            self.m_X = H.T.dot( self.m_X )
        self.eigenValues = e[x]
        self.is_Diagonalized = True


    def rotateY( self ):
        if ( not self.is_Diagonalized ):
            raise lmmError("Diagonalize before rotating Y")
        self.m_allY = self.H.T.dot( self.m_allY )

    def rotateX( self ):
        if ( not self.is_Diagonalized ):
            raise lmmError("Diagonalize before rotating X")
        self.m_X = self.H.T.dot( self.m_X )

    def diagonalize( self ):
        ## First check if the noise covariance is iid
        if ( not self.is_R_iid ):
            self.make_R_iid()

        ## diagonalize the kinship
        e, self.H = linalg.eigh( self.m_G )
        if ( self.m_allY is not None):
            self.m_allY = self.H.T.dot( self.m_allY )
        if ( self.m_X is not None):
            self.m_X = self.H.T.dot(  self.m_X )
        self.eigenValues = e
        i = np.arange( self.n, dtype=int )[ e < 1e-5 ]
        if len(i):
            self.eigenValues[ i ] = 1e-5
        self.is_Diagonalized = True

    def computeConstants( self ):
        self.checkDiagonal()
        self.constTerm = ( self.n - self.p) * ( 1.0 + np.log(2.0 * np.pi) - np.log(self.n - self.p) )
        A = self.m_X.T.dot( self.m_X )
        self.constTerm = self.constTerm - np.log( linalg.det(A) )

    def update( self, d):
        self.checkDiagonal()
        ## Update the terms that are used 
        self.d = float(d)
        self.evals_plus_d = self.d*self.eigenValues + 1.0
        t = 1.0 / ( self.evals_plus_d )
        self.m_Vinv = np.diag( t )

        A = self.m_X.T.dot( self.m_Vinv )
        self.m_Xt_Vinv_X = A.dot( self.m_X ) 
        B = None
        if self.m_Xt_Vinv_X.shape == (1,1):
            B = A / float( self.m_Xt_Vinv_X )
        else:
            B = linalg.solve(self.m_Xt_Vinv_X, A)

        self.m_P = self.m_Vinv - A.T.dot( B )
        tmp = self.m_P.dot( self.m_Y ) 
        self.YtPY = float( self.m_Y.T.dot(tmp) )  
        self.varE = self.YtPY / (self.n - self.p)
        self.varG = self.d * self.varE


    def update_noX( self, d ):
        self.checkDiagonal()
        self.d = float(d)
        self.evals_plus_d = self.d*self.eigenValues + 1.0
        t = 1.0 / ( self.evals_plus_d )
        t.shape = ( self.n, 1 )
        #self.m_Vinv = np.diag( t )

        #self.m_P = self.m_Vinv
        #tmp = self.m_P.dot( self.m_Y )
        #self.YtPY = float( self.m_Y.T.dot( tmp ) )
        tmp = self.m_Y**2 
        self.YtPY = float( tmp.T.dot( t ) )

        self.varE = self.YtPY / (self.n) 
        self.varG = self.d * self.varE


    def LL_noX( self ):
        retVal = self.n * (1.0 + np.log(2.0 * np.pi) ) + np.sum(np.log(self.evals_plus_d))
        retVal = retVal + self.n * np.log(self.YtPY)
        return -0.5 * retVal

    # def dLL_noX( self ):
    #     ##THIS IS OLD NEEDS TO BE UPDATED
    #     A = self.m_P.dot( self.m_Y )
    #     t = 1.0 / self.evals_plus_d;
    #     retVal = np.sum(t) - self.n * float( A.T.dot( A ) ) / float( self.m_Y.T.dot( A ) )
    #     return -0.5 * retVal

    def optLL_noX( self, d ):
        ## I do the search in exp because delta is > 0
        #if ( math.fabs(self.d - d) > 1e-10 ):
        self.update_noX(d)
        return -self.LL_noX()

    # def optdLL_noX( self, d ):
    #     ## I do the search in exp because delta cannot get 0 or less
    #     self.update_noX(d)
    #     #if ( math.fabs(self.d - d) > 1e-10 ):
    #     #    self.update_noX(d)
    #     return -self.dLL_noX()

    def LLR( self ):
        retVal = self.constTerm + np.sum(np.log(self.evals_plus_d)) + np.log( linalg.det( self.m_Xt_Vinv_X ) )
        retVal = retVal + (self.n - self.p) * np.log( self.YtPY )
        return -0.5 * retVal

    # def dLLR( self ):
    #     A = self.m_P.dot( self.m_Y )
    #     retVal = np.trace( self.m_P ) - (self.n - self.p) * float( A.T.dot( A ) ) / float( self.m_Y.T.dot( A ) )
    #     return -0.5 * retVal

    def optLLR( self, d ):
        ## I do the search in exp because delta cannot get 0 or less
        self.update(d)
        #if ( math.fabs(self.d - d) > 1e-3 ):
        #    self.update(d)
        return -self.LLR()

    # def optdLLR( self, d ):
    #     ## I do the search in exp because delta cannot get 0 or less
    #     self.update(d)
    #     if ( math.fabs(self.d - d) > 1e-3 ):
    #         self.update(d)
    #     return -self.dLLR()

    def findOptDelta_noX( self ):
        self.update_noX(1.0)
        optD = optimize.fminbound(self.optLL_noX, 0, 1000, xtol=1e-10, maxfun=5000)
        return optD

    def findOptDelta_noX_withBracket( self, xlow, xhigh ):
        self.update_noX( 0.5*(xlow+xhigh) )
        optD = optimize.fminbound(self.optLL_noX, xlow, xhigh, xtol=1e-10, maxfun=5000)
        return optD


    def findOptDelta( self ):
        self.update(1.0)
        optD, fval, ierr, numF = optimize.fminbound(self.optLLR, 0, 1000, xtol=1e-10, maxfun=5000, full_output=1)
        #print "optD: ", optD, "numF calls: ", numF
        return optD

    def getHeritability( self ):
        ## compute the heritability at d
        n = self.n - 1.0
        N = self.m_C.dot( self.m_G_original )
        t = (np.trace( N ) / n ) * self.varG
        M = self.m_C.dot( self.m_R_original )
        k = (np.trace(M) / n ) * self.varE
        H = t / ( t + k )
        return H

    def getVarExp( self ):
        n = self.n - 1.0
        N = self.m_C.dot( self.m_G_original )
        t = ( np.trace(N) / n ) * self.varG
        M = self.m_C.dot( self.m_R_original )
        k = ( np.trace(M) / n ) * self.varE
        return (t,k) 

    def getBLUP( self, d ):
        ## get the prediction for the genetic component
        ## in the original data
        self.update(d)
        Y = self.m_allY_original[:, self.currentPhenNo]
        Y.shape = (self.n, 1)
        X = np.copy( self.m_X_original )
        V = self.varG * self.m_G_original + self.varE * self.m_R_original 
        VinvX = linalg.solve( V, X )
        XtVinvX = X.T.dot( VinvX )
        XtVinvY = VinvX.T.dot( Y )
        beta = None
        if XtVinvX.shape == (1,1):
            a = float( XtVinvX )
            beta = ( 1.0 / a ) * float( XtVinvY )
        else:
            beta = linalg.solve( XtVinvX, XtVinvY )
        y_Xb = None
        if type(beta) is float:
            y_Xb = Y - float(beta)*X
        else:
            y_Xb = Y - X.dot( beta )
        tmp = linalg.solve( V, y_Xb )
        blup = self.varG * self.m_G_original.dot( tmp )
        return blup

    def sweep( self ):
        N = 500
        out = np.empty((3, N))
        out[0,] = np.linspace(1e-3, 10, num=N)
        i = 0
        for delta in out[0,]:
            self.update(delta)
            out[1, i] = self.LLR()
            out[2, i] = self.dLLR()
            i = i + 1
        return out

    def sweep_noX( self ):
        N = 500
        out = np.empty((3, N))
        out[0,] = np.linspace(1e-3, 50, num=N)
        i = 0
        for delta in out[0,]:
            self.update_noX(delta)
            out[1, i] = self.LL_noX()
            out[2, i] = self.dLL_noX()
            i = i + 1
        return out

    def LLR2D( self, K, n ):
        """
        n: which phenotype, K kernel
        """
        self.readG_soft(K)
        self.setY_fromOriginal(n)
        self.setX_fromOriginal()
        self.diagonalize()
        self.setY(n)
        self.computeConstants()
        self.update(1.0)
        optD = self.findOptDelta()
        self.update(optD)
        LLR = self.LLR()
        return (optD, LLR)

    def findOptDelta2D( self, regionKernel, backgroundKernel, n ):
        """
        Binary search to max. LL on w, w*K_r + (1-w)*Kg
        """
        tol = 1e-5
        K_r = np.copy( regionKernel )
        K_g = np.copy( backgroundKernel )
       
        w_a = 0.0
        w_b = 1.0

        LL_a = 0.0
        optD_a, LL_a = self.LLR2D(K_g, n)
        LL_null = LL_a
        print """LL_null: %.3f""" % ( LL_null )
        grat = 0.61803398875
        w_1 = grat * w_a + (1.0 - grat) * w_b
        w_2 = (1.0 - grat) * w_a + grat * w_b
        optD_1, LL_1 = self.LLR2D(w_1 * K_r + (1.0 - w_1) * K_g, n)
        optD_2, LL_2 = self.LLR2D(w_2 * K_r + (1.0 - w_2) * K_g, n)
        w_opt = 0.0
        err = 100
        numSteps = 0
        LL_opt = 0.0
        while( (err > tol) and (numSteps < 20) ):
            numSteps += 1
            print """w_a: %.5f w_1: %.5f w_2: %.5f w_b: %.5f""" % ( w_a, w_1, w_2, w_b )
            print """err: %.5f\tw_1: %.5f\tLL_1: %.3f\tw_2: %.5f\tLL_2: %.3f""" % ( err, w_1, LL_1, w_2, LL_2 )
            if ( LL_1 > LL_2 ):
                w_opt = w_1
                LL_opt = LL_1
                optD_opt = optD_1
                w_b = w_2
                w_2 = w_1
                LL_2 = LL_1
                w_1 = grat * w_a + (1.0 - grat) * w_b
                optD_1, LL_1 = self.LLR2D(w_1 * K_r + (1.0 - w_1) * K_g, n)

            else:
                w_opt = w_2
                LL_opt = LL_2
                optD_opt = optD_2
                w_a = w_1
                w_1 = w_2
                LL_1 = LL_2
                w_2 = (1.0 - grat) * w_a + grat * w_b
                optD_2, LL_2 = self.LLR2D(w_2 * K_r + (1.0 - w_2) * K_g, n)
            err = w_b - w_a
        self.mat
        return (LL_null, LL_opt, w_opt, optD_opt)

    def findOptDelta2D_grid( self, regionKernel, backgroundKernel, n):
        """
        Grid search on w
        """
        K_r = regionKernel.copy()
        K_g = backgroundKernel.copy()

        ws = np.linspace(0.0, 1.0, 50)
        LLs = []
        optDs = []
        for w in ws:
            optD, LL = self.LLR2D(w * K_r + (1.0 - w) * K_g, n)
            optDs.append(optD)
            LLs.append(LL)
            print w, LL
        return ( ws, LLs, optDs)


